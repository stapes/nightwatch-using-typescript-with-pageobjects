// require('dotenv').config();

import * as dotenv from "dotenv";

dotenv.config();

module.exports = {
'User can sign in'(client) {
    const signinPage = client.page.signinPage();
    const instancesPage = client.page.instancesPage();
	
	console.log("Sign in as email: " + process.env.EMAIL);
	
    signinPage
      .navigate()
      .signin(process.env.EMAIL, process.env.PASSWORD);

	  // give it time to load
	  
	client.pause(5000);
	  
    instancesPage.expect.element('@homepageWelcomeTitle').text.to.contain('Welcome to the CJDocs Home!');

    client.end();
}
}