"use strict";
// require('dotenv').config();
Object.defineProperty(exports, "__esModule", { value: true });
var dotenv = require("dotenv");
dotenv.config();
module.exports = {
    'User can sign in': function (client) {
        var signinPage = client.page.signinPage();
        var instancesPage = client.page.instancesPage();
        console.log("Sign in as email: " + process.env.EMAIL);
        signinPage
            .navigate()
            .signin(process.env.EMAIL, process.env.PASSWORD);
        // give it time to load
        client.pause(5000);
        instancesPage.expect.element('@homepageWelcomeTitle').text.to.contain('Welcome to the CJDocs Home!');
        client.end();
    }
};
//# sourceMappingURL=signin.spec.js.map